import java.util.ArrayList;

public class Main {
    public static class Mahasiswa {
        String nama;
        int nim;
        String prodi;
        int tahunmasuk;
        ArrayList<Transkrip> transkrip = new ArrayList<>();
    }

    public static class Kursus {
        String nama;
        int kodematkul;
        int sks;
    }

    public static class Transkrip {
        Mahasiswa mhs;
        Kursus kursus;
        int nilai;
    }

    public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip) {
        mahasiswa.transkrip.add(transkrip);
    }

    public static void title(Mahasiswa mhs) {
        System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
        for (Transkrip transcript : mhs.transkrip) {
            cetakTranscript(transcript);
        }
    }

    public static void cetakTranscript(Transkrip transcript) {
        System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
    }

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.nama = "Alex";
        mhs1.nim = 235150707;
        mhs1.prodi = "TI";
        mhs1.tahunmasuk = 2022;

        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.nama = "Habibi";
        mhs2.nim = 122121;
        mhs2.prodi = "Sistem Informasi";
        mhs2.tahunmasuk = 2020;

        Kursus mk1 = new Kursus();
        mk1.nama = "Pemrograman Lanjut";
        mk1.kodematkul = 101;
        mk1.sks = 3;

        Kursus mk2 = new Kursus();
        mk2.nama = "Sistem Basis Data";
        mk2.kodematkul = 111;
        mk2.sks = 2;

        Transkrip t1 = new Transkrip();
        t1.mhs = mhs1;
        t1.kursus = mk1;
        t1.nilai = 88;

        Transkrip t2 = new Transkrip();
        t2.mhs = mhs1;
        t2.kursus = mk2;
        t2.nilai = 93;

        Transkrip t3 = new Transkrip();
        t3.mhs = mhs2;
        t3.kursus = mk1;
        t3.nilai = 90;

        Transkrip t4 = new Transkrip();
        t4.mhs = mhs2;
        t4.kursus = mk2;
        t4.nilai = 92;

        addTranscript(mhs1, t1);
        addTranscript(mhs1, t2);
        title(mhs1);
        addTranscript(mhs2, t3);
        addTranscript(mhs2, t4);
        title(mhs2);
    }
}
